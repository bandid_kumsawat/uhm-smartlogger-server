var router = require('express').Router();
var mongoose = require('mongoose');
var Data = mongoose.model('Datas');
var DataIndex = mongoose.model('DataIndex');
// var auth = require('../auth');
// var underjs = require('underscore');
var moment = require('moment');
// var stringify = require('csv-stringify');

function cvtTimetoUTC(gmt_time){
  console.log(gmt_time)
  //
  var utc_time = moment(gmt_time).utcOffset(7);
  console.log(utc_time.utc().format("YYYY-MM-DDTHH:mm:ss.SSS"))
  return utc_time.utc().format("YYYY-MM-DDTHH:mm:ss.SSS");
}

router.get('/', function (req, res, next) {
  console.log(req.query.ed);
  
  if(typeof req.query.st === undefined){
    return res.status(422).json({error:"no start Time"})
  }

  if(typeof req.query.ed === undefined){
    return res.status(422).json({error:"no end Time"})
  }

  var st_time = new Date(req.query.st)
  var ed_time = new Date(req.query.ed)

  if( ed_time < st_time)  {
    return res.status(422).json({error:"no Wrong Interval"})
  }

  var qry = {
    id: req.query.id,
    Time: {
      $lte: (req.query.ed),
      $gte: (req.query.st)
    }
  };

  console.log(qry);
  //return res.json(qry);
  Data.find(qry).then(function (ret) {
    console.log("!! Result Search :" + ret.length)

    return res.json(ret);
  }).catch(next);

});


router.get('/utc', function (req, res, next) {
  console.log(req.query.ed);

  var qry = {
    id: req.query.id,
    UTC: {
      $lte: req.query.ed,
      $gte: req.query.st
    }
  };
  console.log(qry);

  Data.find(qry).then(function (ret) {
    console.log("!! Result Search :" + ret.length)
    return res.json(ret);
  }).catch(next);

});

router.get('/index', function (req, res, next) {
  
  var t_period = 1; // default

  if(typeof req.query.id === undefined){
    return res.status(422).json({error:"no ID"})
  }

  if(typeof req.query.period !== undefined){
    t_period = req.query.period 
  }

  if(typeof req.query.st === undefined){
    return res.status(422).json({error:"no start Time"})
  }

  if(typeof req.query.ed === undefined){
    return res.status(422).json({error:"no end Time"})
  }

  var qry = {
    id: req.query.id,
    Time: {
      $lte: req.query.ed,
      $gte: req.query.st
    }
  };
 
    DataIndex.find(qry).then(function(ret){

    var len = ret.length;

    console.log("Ret :"+len)
    var data_index=[];
    var data_group=[];
    
    var count=1;

    for(var i=0;i<len;i++){
      var n_time = ret[i].Time
      data_index.push(n_time)
    }

    var count = 1;
    var f_time_str = data_index[0]
    var f_time = moment(f_time_str).valueOf();
    var tmp_group = [data_index[0]];

    for(var j=1;j<len;j++){

      chk_time_str = data_index[j]
      chk_time = moment(chk_time_str).valueOf();
      
      if(chk_time < f_time+(1000*t_period)){
        tmp_group.push(data_index[j]);
      }else{
        // insert New Group
        data_group.push(
          {
            index:count,
            f_time:f_time_str,
            l_time: data_index[j-1],
            data:tmp_group,
          })
        count++;

        //reset count
        f_time_str = chk_time_str
        f_time = chk_time;
        tmp_group =[data_index[j]]
      }

      if(j == len-1){
        data_group.push(
          {
            index:count,
            f_time:f_time_str,
            l_time: data_index[j-1],
            data:tmp_group,
        })
      }

    }

    var ret_msg = {
        data_count:len,
        period:t_period,
        st_time:req.query.st,
        ed_time:req.query.ed,
        t_index:data_index,
        d_group:data_group,
    }
    return res.json(ret_msg);

  }).catch(next);
});


router.get('/last', function (req, res, next) {
  var qry = { id: req.query.id };


  /*
  Data.findOne(qry).sort({ Time: -1 }).exec(function (err, ret) {
    return res.json(ret);
  }).catch(next);
*/
  Data.findOne(qry).sort({ _id: -1 }).exec(function (err, ret) {
    return res.json(ret);
  }).catch(next);

});

module.exports = router;