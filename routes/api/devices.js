var router = require('express').Router();
var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Device = mongoose.model('Device');
var auth = require('../auth');
var Datas = mongoose.model('Datas');
var moment = require('moment')
var momentTz = require('moment-timezone')

router.get('/', function(req, res, next) {
  Device.find({}).then(function(ret){
    //console.log(ret)
    return res.json(ret);
  }).catch(next);
});

router.post('/add', function(req, res, next) {
    
    var dev = new Device();

    // dev.id = req.body.device.id;
    dev.lat = ""
    dev.long = ""
    dev.stream = 0
    dev.info =""
    dev.threshole_low = 20
    dev.threshole_upper = 30
    dev.temp = "0.0"
    Device.find({}).sort({
      id: -1 
     }).then(function(result,err){
     
  
      var id_srt = result[0].id
  
      id_srt = id_srt.substring(id_srt.length-6 ,id_srt.length)
  
  
      var str = "" + (parseInt(id_srt) +1)
      var pad = "000000"
      var id = pad.substring(0, pad.length - str.length) + str
  
      //console.log(id_srt);
      //console.log("WS"+id);
      dev.id = "WS"+id;
      if (req.body.device.info !=='undefined'){
        dev.info = req.body.device.info;
      }
      
      dev.save().then(function(){
        return res.json({ret:"ok"});
      }).catch(next);


  
     });


});

// update device
router.post('/update', function(req, res, next) {

    var check_para = {};

    //console.log(req.body)

    if (req.body.time !== undefined){
      var strtime = req.body.time
      // new Date(year, month, day, hours, minutes, seconds, milliseconds)
      var Dtime = new Date(
        strtime.substr(0, 4), 
        strtime.substr(4, 2), 
        strtime.substr(6, 2), 
        strtime.substr(8, 2), 
        strtime.substr(10, 2), 
        strtime.substr(12, 2)
      );
      // 2020 03 29 11 34 34 
      // 2021 03 29 12 03 25
      var dd = Dtime.toISOString()
      check_para.timestamp = dd;
    }

    if (req.body.temp !== undefined){
      // console.log(req.body.temp)
      // console.log((((24+25) * (req.body.temp*100/(4.192-0.431))) / 100) - 25)
      check_para.temp = ((((24+25) * (req.body.temp*100/(4.192-0.431))) / 100) - 25).toFixed(2)
    }

    if (req.body.Flow !== undefined){
      check_para.Flow = req.body.Flow
    }

    if (req.body.Flow_Accumulate !== undefined){
      check_para.Flow_Accumulate = req.body.Flow_Accumulate
    }


    if (req.body.Pressure_1 !== undefined){
      check_para.Pressure_1 = req.body.Pressure_1
    }


    if (req.body.Pressure_2 !== undefined){
      check_para.Pressure_2 = req.body.Pressure_2
    }

    if (req.body.batt !==undefined){
      check_para.batt = req.body.batt;
    }

    if (req.body.lat !==undefined){
      check_para.lat = req.body.lat;
    }

    if (req.body.long !==undefined){
      check_para.long = req.body.long;
    }

    if (req.body.info !==undefined){
      check_para.info = req.body.info;
    }

    if (req.body.stream !==undefined){
      check_para.stream = req.body.stream;
    }

    if (req.body.gps_count !==undefined){
      check_para.gps_count = req.body.gps_count;
    }

    if (req.body.tel !==undefined){
      check_para.tel = req.body.tel;
    }

    if (req.body.tel_op !==undefined){
      check_para.tel_op = req.body.tel_op;
    }

    if (req.body.threshole_low !==undefined){
      check_para.threshole_low = req.body.threshole_low;
    }

    if (req.body.threshole_upper !==undefined){
      check_para.threshole_upper = req.body.threshole_upper;
    }

    // var nowtime = moment().toISOString(true);//moment(new Date()).zone('+0700').toISOString()

    // var dd = new Date().toISOString()
    // //console.log(dd);
    
    // check_para.timestamp = dd;

    var update_json      = {$set:check_para}

    Device.findOneAndUpdate({id:req.body.id}, update_json,{new: true}, (err, doc) => {
      if (err) {
          //console.log("Something wrong when updating data!");
          return res.json({ret:"fail"});
      }

      if (doc === null){
        save(req.body.id, check_para, next, res)
      }else {
        if (req.body.isUpdate === undefined){
          var data = new Datas();

          data.id = req.body.id;
          data.Time = new Date(dd);
          data.Flow = check_para.Flow
          data.Flow_Accumulate = check_para.Flow_Accumulate
          data.Pressure_1 = check_para.Pressure_1
          data.Pressure_2 = check_para.Pressure_2
          data.Batt = check_para.batt
          data.save().then(function(){
            return res.json({ret:"OK"})
          }).catch(next)
        } else {
          return res.json({ret:"OK"})
        }
      }

    });

});

// Delete Device
router.delete('/',  function(req, res, next) {

    //console.log(req.body.id);
    
    if(typeof req.body.id === 'undefined'){
      return res.status(422).json({error:"no device_id"})
    }

    Device.remove({ id: req.body.id }, function(err) {
      if (!err) {
        return res.json({ret:"delete Success"});
      }
      else {
        return res.json({ret:"delete fail"});
      }
    });
});


router.post("/log", (req, res, next) => {
  var current = new Date()
  current.setHours(current.getHours() - 1)
  console.log(current)
  var qry = {
    id: req.body.id,
    Time: {
      $gte: current,
    }
  }

  Datas.find(qry).then((result) => {
    return res.json(result)
  })
})


router.post("/sensorlog", (req, res, next) => {
  var current = new Date()
  current.setDate(current.getDay() - 2)
  var qry = {
    id: req.body.id,
    Time: {
      $gte: new Date(req.body.start_date + "T00:00:00.000Z"),
      $lte: new Date(req.body.stop_date + "T23:59:59.999Z")
    }
  }
  Datas.find(qry).then((result) => {
    return res.json({
      result: result,
      length: result.length
    })
  })
})

function save(req_id,data,next,res){
  var dev = new Device();

  dev.id = req_id
  dev.lat = "13.8849612"
  dev.long = "100.5514425"
  dev.info = ''
  dev.batt = data.batt
  dev.gps_count = 0
  dev.stream = ''
  dev.tel = ''
  dev.tel_op = ''
  dev.timestamp = data.timestamp
  dev.Flow = data.Flow
  dev.Flow_Accumulate = data.Flow_Accumulate
  dev.Pressure_1 = data.Pressure_1
  dev.Pressure_2 = data.Pressure_2

  dev.save().then(function(){
    return res.json({ret:"ok"});
  }).catch(next); 
  
}

module.exports = router;