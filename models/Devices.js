var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var DeviceSchema = new mongoose.Schema({
    id: {type: String, unique: true},
    lat :String,
    long :String,
    info :String,
    batt :String,
    gps_count:Number,
    stream:Number,
    tel:String,
    tel_op:String,
    Schedule:[Object],
    timestamp:Date,
    threshole_low:String,
    threshole_upper:String,
    temp: String,
    Flow: String,
    Flow_Accumulate: String,
    Pressure_1 : String,
    Pressure_2: String,
  });

  DeviceSchema.plugin(uniqueValidator, {message: 'is already taken.'});


  DeviceSchema.methods.toJSONFor = function(user){
    return {
      lat :this.lat,
      long :this.long,
      info :this.info,
      batt :this.batt,
      Schedule:this.Schedule
    };
  };

  mongoose.model('Device', DeviceSchema);
  
