var http = require('http'),
    path = require('path'),
    express = require('express'),
    session = require('express-session'),
    cors = require('cors'),
    moment = require('moment'),
    mqtt = require('mqtt');

var isProduction = process.env.NODE_ENV === 'production';

// Create global app object
var app = express();

app.use(cors());

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (!isProduction) {
  app.use(function(err, req, res, next) {
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({'errors': {
      message: err.message,
      error: err
    }});
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({'errors': {
    message: err.message,
    error: {}
  }});
});

// finally, let's start our server...
var server = app.listen( process.env.PORT || 2181, function(){
  console.log('Listening on port ' + server.address().port);
});

const io = require("socket.io")(server, {
  handlePreflightRequest: (req, res) => {
      const headers = {
          "Access-Control-Allow-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
          "Access-Control-Allow-Credentials": true
      };
      res.writeHead(200, headers);
      res.end();
  }
});

var users = 0

//listen on every connection
io.on('connection', (socket) => {
console.log("isConnect");
  var clientMQTT = mqtt.connect("mqtt://nrwiot.pwa.co.th",{
    port: 1883,
    host: 'mqtt://nrwiot.pwa.co.th',
    clientId: 'MQTTJS____WEB____client_IN__SERVER_' + Math.random().toString(16).substr(2, 8),
    username: 'admin',
    password: '2020iotdmamqtt',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
  })
  var topic_list=["SCADA/#"];// SCADA/REG1/5531011/KS/DPB/1/FLOW
  //var topic_list=["SCADA/#"];// SCADA/REG1/5531011/KS/DPB/1/FLOW
  clientMQTT.subscribe(topic_list,{qos:2});
  socket_con = socket
  users++
	console.log('New user connected ' + (users))

  socket.on('disconnect', (socket) => {
    clientMQTT.end();
    console.log("disconnect")
    console.log(socket)
    users--
  });


  //handle incoming messages
  clientMQTT.on('message',function(topic, message, packet){
    console.log("message is "+ message);
    console.log("topic is "+ topic);
    let json = JSON.stringify(message);
    let bufferOriginal = Buffer.from(JSON.parse(json).data);
    var res = [
      {
        tag: topic,
        val: bufferOriginal.toString('utf8')
      },{
        tag: 'datetime',
        val: moment(new Date()).format('MMMM DD YYYY, HH:mm:ss')
      }
    ]
    console.log("send to client id : " + socket.id)
    console.log(res)
    io.sockets.connected[socket.id].emit('sensor', res);
  });

})
